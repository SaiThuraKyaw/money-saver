package com.example.saithurakyaw.moneysaver;


import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class ReportFragment extends Fragment {

    private TextView et_datePicker, totalIncome, totalExpense;
    private String monthYearStr, currentDate, sDate, eDate;
    private int tYear, tMonth;
    private double income, expense;

    private List<Entry> entryList = new ArrayList<>();
    private java.util.List<Category> categoryList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ReportAdapter reportAdapter;
    private DatabaseHandler databaseHandler;
    private Date dt;

    SimpleDateFormat sdf = new SimpleDateFormat("MMM yyyy");
    SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd");
    SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("yyyy-M");
    SimpleDateFormat dateFormatYear = new SimpleDateFormat("yyyy");
    SimpleDateFormat dateFormatMonth = new SimpleDateFormat("M");
    SimpleDateFormat dateFormatToday = new SimpleDateFormat("yyyy-M-d");

    public ReportFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_report, container, false);

        et_datePicker = (TextView) v.findViewById(R.id.et_datePicker);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        totalIncome = (TextView) v.findViewById(R.id.totalIncome);
        totalExpense = (TextView) v.findViewById(R.id.totalExpense);

        income = 0.00;
        expense = 0.00;

        dt = new java.util.Date();
        currentDate = sdf.format(dt);
        et_datePicker.setText(currentDate);

        databaseHandler = new DatabaseHandler(v.getContext());

        sDate = simpleDateFormat.format(dt).concat("-1");
        //eDate = simpleDateFormat.format(dt).concat("-31");
        eDate = dateFormatToday.format(dt);
        tYear = Integer.valueOf(dateFormatYear.format(dt));
        tMonth = Integer.valueOf(dateFormatMonth.format(dt));

        entryList = databaseHandler.getEntryByMonth(sDate, eDate);
        categoryList = databaseHandler.getAllCategory();

        Log.d("Date Range", String.valueOf(entryList.size()));

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        reportAdapter = new ReportAdapter(entryList, categoryList);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(v.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(reportAdapter);
        addIncomeAndExpense(entryList);
        totalExpense.setText(String.valueOf(expense));
        totalIncome.setText(String.valueOf(income));

        et_datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MonthYearPickerDialog pickerDialog = new MonthYearPickerDialog();
                pickerDialog.setListener(new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int year, int month, int i2) {
                        expense = 0.0;
                        income = 0.0;
                        monthYearStr = year + "-" + (month + 1) + "-" + i2;
                        et_datePicker.setText(formatMonthYear(monthYearStr));
                        Toast.makeText(getActivity(), year + "-" + (month + 1) + "-" + i2, Toast.LENGTH_SHORT).show();
                        sDate = year + "-" + month + "-1";
                        if(tYear == year && tMonth == month)
                        {
                            eDate = dateFormatToday.format(dt);
                        }
                        else{
                            eDate = year + "-" + month + "-31";
                        }
                        entryList = databaseHandler.getEntryByMonth(sDate,eDate);
                        addIncomeAndExpense(entryList);
                        reportAdapter.swapItem(entryList);

                        totalExpense.setText(String.valueOf(expense));
                        totalIncome.setText(String.valueOf(income));
                        Toast.makeText(getActivity(), "List Size: " + entryList.size(), Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getActivity(), "Expense: "+expense + "\n Income: " + income,Toast.LENGTH_SHORT).show();
                    }
                });
                //pickerDialog.show(getSupportFragmentManager(), "MonthYearPickerDialog");
                pickerDialog.show(getFragmentManager(), "MonthYearPickerDialog");
            }
        });
        return v;
    }

    String formatMonthYear(String str) {
        Date date = null;
        try {
            date = input.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sdf.format(date);
    }

    public int  findCategoryType(int categoryId, List<Category> categories){
        for (Category category : categories) {
            if (category.getCategoryId() == categoryId) {
                return category.isExpense();
            }
        }
        return -1;
    }

    public void addIncomeAndExpense(List<Entry> entries){
        for(Entry entry:entries) {
            if (findCategoryType(entry.getCategoryId(), categoryList) == 1) {
                expense += entry.getAmount();
            } else {
                income += entry.getAmount();
            }
        }

    }

}
