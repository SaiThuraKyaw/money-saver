package com.example.saithurakyaw.moneysaver;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ReportAdapter extends RecyclerView.Adapter<ReportAdapter.MyViewHolder>{

    private List<Entry> entryList;
    private List<Category> categoryList;
    private Category foundCategory;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name, report_category, amount;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            report_category = (TextView) view.findViewById(R.id.report_category);
            amount = (TextView) view.findViewById(R.id.amount);
        }
    }

    public ReportAdapter(List<Entry> entryList) {
        this.entryList = entryList;
    }

    public ReportAdapter(List<Entry> entryList, List<Category> categoryList) {
        this.entryList = entryList;
        this.categoryList = categoryList;
    }

    @Override
    public ReportAdapter.MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.report_list_row, viewGroup, false);
        return new ReportAdapter.MyViewHolder((itemView));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Entry entry = entryList.get(i);
        //myViewHolder.Report.setText(entry.getEntryName());
        foundCategory = findCategory(entry.getCategoryId(),categoryList);
        myViewHolder.report_category.setText(foundCategory.getCategoryName());
        if(foundCategory.isExpense()==1){
            myViewHolder.amount.setTextColor(0xffff2603);
        }
        else{
            myViewHolder.amount.setTextColor(0xff00c91d);
        }
        myViewHolder.amount.setText((String.valueOf(entry.getAmount())));

    }

    @Override
    public int getItemCount() {
        return entryList.size();
    }

    public Category  findCategory(int categoryId, List<Category> categories){
        for (Category category : categories) {
            if (category.getCategoryId() == categoryId) {
                return category;
            }
        }
        return null;
    }

    public void swapItem(List<Entry> entries){
        final EntryDiffCallback entryDiffCallback = new EntryDiffCallback(this.entryList, entries);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(entryDiffCallback);

        this.entryList.clear();
        this.entryList.addAll(entries);

        diffResult.dispatchUpdatesTo(this);
    }
}
