package com.example.saithurakyaw.moneysaver;

import android.support.v7.util.DiffUtil;

import java.util.List;

public class EntryDiffCallback extends DiffUtil.Callback {

    private final List<Entry> oldList;
    private final List<Entry> newList;

    public EntryDiffCallback(List<Entry> oldList, List<Entry> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getEntryId() == newList.get(newItemPosition).getEntryId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        /**
        Entry oldEntry = oldList.get(oldItemPosition);
        Entry newEntry = newList.get(newItemPosition);
        if(oldEntry.getEntryName() == newEntry.getEntryName() && oldEntry.getAmount() == newEntry.getAmount()){
            return true;
        }**/
        return false;
    }
/**
    @Nullable
    @Override
    public Object getChangePayload(int oldItemPosition, int newItemPosition) {
        // Implement method if you're going to use ItemAnimator
        return super.getChangePayload(oldItemPosition, newItemPosition);
    }**/
}
