package com.example.saithurakyaw.moneysaver;

import android.support.v7.util.DiffUtil;

import java.util.List;

public class CategoryDiffCallBack extends DiffUtil.Callback {

    private final List<Category> oldList;
    private final List<Category> newList;

    public CategoryDiffCallBack(List<Category> oldList, List<Category> newList) {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize() {
        return oldList.size();
    }

    @Override
    public int getNewListSize() {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return oldList.get(oldItemPosition).getCategoryId() == newList.get(newItemPosition).getCategoryId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
