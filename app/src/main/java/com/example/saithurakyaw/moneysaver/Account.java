package com.example.saithurakyaw.moneysaver;

public class Account {
    private int accountId;
    private String accountName;
    private double amount;

    public Account() {
    }

    public Account(int accountId) {
        this.accountId = accountId;
    }

    public Account(int accountId, String accountName, double amount) {
        this.accountId = accountId;
        this.accountName = accountName;
        this.amount = amount;
    }

    public Account(String accountName, double amount) {
        this.accountName = accountName;
        this.amount = amount;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
