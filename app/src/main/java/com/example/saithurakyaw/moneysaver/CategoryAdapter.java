package com.example.saithurakyaw.moneysaver;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.MyViewHolder> {

    private List<Category> categoryList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView categoryName, categoryAmount;


        public MyViewHolder(View view) {
            super(view);
            categoryName = (TextView) view.findViewById(R.id.categoryName);
        }
    }

    public CategoryAdapter(List<Category> categoryList) {
        this.categoryList = categoryList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.category_list_row, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Category category = categoryList.get(i);
        myViewHolder.categoryName.setText(category.getCategoryName());
    }

    @Override
    public int getItemCount() {
        return categoryList.size();
    }

    public void swapItem(List<Category> categories){
        final CategoryDiffCallBack categoryDiffCallBack = new CategoryDiffCallBack(this.categoryList, categories);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(categoryDiffCallBack);

        this.categoryList.clear();
        this.categoryList.addAll(categories);
        diffResult.dispatchUpdatesTo(this);
    }
}
