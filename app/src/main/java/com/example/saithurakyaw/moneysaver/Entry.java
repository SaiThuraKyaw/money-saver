package com.example.saithurakyaw.moneysaver;

import java.util.Date;

public class Entry {

    private int entryId;
    private String entryName;
    private int categoryId;
    private int accountId;
    private String entryDate;
    private double amount;
    private String note;

    public Entry() {
    }

    public Entry(int entryId, String entryName, int categoryId, int accountId, String entryDate, double amount, String note) {
        this.entryId = entryId;
        this.entryName = entryName;
        this.categoryId = categoryId;
        this.accountId = accountId;
        this.entryDate = entryDate;
        this.amount = amount;
        this.note = note;
    }

    public Entry(String entryName, int categoryId, int accountId, String entryDate, double amount, String note) {
        this.entryName = entryName;
        this.categoryId = categoryId;
        this.accountId = accountId;
        this.entryDate = entryDate;
        this.amount = amount;
        this.note = note;
    }

    public int getEntryId() {
        return entryId;
    }

    public void setEntryId(int entryId) {
        this.entryId = entryId;
    }

    public String getEntryName() {
        return entryName;
    }

    public void setEntryName(String entryName) {
        this.entryName = entryName;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(String entryDate) {
        this.entryDate = entryDate;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}
