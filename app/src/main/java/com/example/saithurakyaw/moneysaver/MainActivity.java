package com.example.saithurakyaw.moneysaver;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;


public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public BottomNavigationView navigationView;
    CalendarFragment calendarFragment;
    CategoryFragment categoryFragment;
    AccountFragment accountFragment;
    ReportFragment reportFragment;
    GestureDetector gestureDetector;
    private FragmentManager fragmentManager = getSupportFragmentManager();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        //FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        calendarFragment = new CalendarFragment();
        fragmentTransaction.replace(R.id.container, calendarFragment);
        fragmentTransaction.commit();

        navigationView.setOnNavigationItemSelectedListener(this);


        DatabaseHandler databaseHandler = new DatabaseHandler(this);

/**
        //Add data to Account Table
        Log.d("Insert: ", "Add data to Account Table");
        databaseHandler.addAccount(new Account("Default", 1000));
        databaseHandler.addAccount(new Account("Bank", 1000.50));

        //Add Data to Category
        Log.d("Insert:" , "Add Data to Category" );
        databaseHandler.addCategory(new Category("Eating Out", 1));
        databaseHandler.addCategory(new Category("Groceries", 1));
        databaseHandler.addCategory(new Category("Medical",1));
        databaseHandler.addCategory(new Category("Transport",1));
        databaseHandler.addCategory(new Category("Utilities",1));
        databaseHandler.addCategory(new Category("Salary",0));
        databaseHandler.addCategory(new Category("Bonus",0));


        //Add Data to Entry
        Log.d("Insert: ", "Add Data to Entry");
        databaseHandler.addEntryList(new Entry("Taxi", 4, 1, "2018-12-1", -10.0, ""));
        databaseHandler.addEntryList(new Entry("Dinner", 1, 1, "2018-12-30", -3.00, ""));
        databaseHandler.addEntryList(new Entry("Breakfast", 1, 1, "2018-12-31", -3.00, ""));
        databaseHandler.addEntryList(new Entry("Lunch", 1, 1, "2018-12-31", -15.0, "Entry 2"));
        databaseHandler.addEntryList(new Entry("Salary", 6, 1, "2018-12-31", 1000.0, ""));
        databaseHandler.addEntryList(new Entry("Breakfast", 1, 1, "2019-1-31", -3.50, ""));
        databaseHandler.addEntryList(new Entry("Lunch", 1, 1, "2019-1-31", -5.0, "Entry 2"));
        databaseHandler.addEntryList(new Entry("Coffee", 1, 1, "2019-2-1", -5.0, "Entry 2"));
        //databaseHandler.addEntryList(new Entry("Taxi", 4, 1, "2018-12-01", 10.0, ""));

        databaseHandler.addEntryList(new Entry("Breakfast", 1, 1, "2019-2-6", -3.0, "Entry 2"));
        databaseHandler.addEntryList(new Entry("Lunch", 1, 1, "2019-2-6", -6.0, "Entry 2"));
        databaseHandler.addEntryList(new Entry("Dinner", 1, 1, "2019-2-6", -15.0, "Entry 2"));

/**
        databaseHandler.addEntryList(new Entry("Utilities", 5, 1, "2019-2-6", -100.0, "Entry 2"));
        databaseHandler.addEntryList(new Entry("Income", 6, 1, "2019-2-6", 100.0, "Entry 2"));
        **/

        //databaseHandler.deleteEntryList(new Entry(8,"Salary", 6, 1, "2018-12-31", 1000.0, ""));
        //databaseHandler.deleteEntryList(new Entry(9,"Salary", 6, 1, "2018-12-31", 1000.0, ""));
        Log.d("Reading:" , "Reading All Account......");
        List<Account> accountList = databaseHandler.getAllAccount();
        Log.d("Account List Size", String.valueOf(accountList.size()));
        for(Account account : accountList){
            String log = "Account ID: " +account.getAccountId() +
                    ", Account Name: " + account.getAccountName() +
                    ", Amount " + account.getAmount();
            Log.d("Account :" ,log);
        }
        Log.d("Done", "Reading Done");

        //databaseHandler.deleteAccount(new Account(3));
        //databaseHandler.deleteAccount(new Account(4));




        Log.d("Reading", "Reading All Category");
        List<Category> categoryList = databaseHandler.getAllCategory();
        Log.d("Category List Size", String.valueOf(categoryList.size()));
        for(Category category : categoryList){
            String log = "Category ID: " + category.getCategoryId() +
                    " Category Name: " + category.getCategoryName() +
                    "Is Expense " + category.isExpense();
            Log.d("Category :", log);
        }
        Log.d("Done", "Reading Done");

        Log.d("Get Category By Expense", "#################################################################");
        categoryList = databaseHandler.getCategoryByType(1);
        Log.d("Category List Size", String.valueOf(categoryList.size()));
        for(Category category : categoryList){
            String log = "Category ID: " + category.getCategoryId() +
                    " Category Name: " + category.getCategoryName() +
                    "Is Expense " + category.isExpense();
            Log.d("Category :", log);
        }
        Log.d("Done", "Reading Done");

        //databaseHandler.updateCategory(new Category(1, "Eating"));
        //databaseHandler.deleteCategory(10);
        Log.d("Reading", "Reading All Category");
        categoryList = databaseHandler.getAllCategory();
        Log.d("Category List Size", String.valueOf(categoryList.size()));
        for(Category category : categoryList){
            String log = "Category ID: " + category.getCategoryId() +
                    " Category Name: " + category.getCategoryName();
            Log.d("Category :", log);
        }
        Log.d("Done", "Reading Done");



        //databaseHandler.deleteEntryList(new Entry(2,"Lunch", 2, 2, "2019-02-1", 7.0, "Entry 4"));
        Log.d("Reading", "Reading All Entry");
        List<Entry> entryList = databaseHandler.getAllEntryList();
        Log.d("Entry List Size", String.valueOf(entryList.size()));
        for(Entry entry : entryList){
            String log = "Entry ID:  " + entry.getEntryId() +
                    " Entry Name: " + entry.getEntryName() +
                    " Category ID: " + entry.getCategoryId() +
                    " Account ID: " + entry.getAccountId() +
                    " Entry Date: " + entry.getEntryDate() +
                    " Entry Amount: " + entry.getAmount() +
                    " Entry Note: " + entry.getNote();
            Log.d("Entry: ", log);
        }

        Log.d("Done", "Reading Done");

        Log.d("Reading", "Reading Entry From 2019-01-31");
        entryList = databaseHandler.getEntryListByDate("2019-01-31");
        Log.d("Entry List Size", String.valueOf(entryList.size()));
        for(Entry entry : entryList){
            String log = "Entry ID:  " + entry.getEntryId() +
                    " Entry Name: " + entry.getEntryName() +
                    " Category ID: " + entry.getCategoryId() +
                    " Account ID: " + entry.getAccountId() +
                    " Entry Date: " + entry.getEntryDate() +
                    " Entry Amount: " + entry.getAmount() +
                    " Entry Note: " + entry.getNote();
            Log.d("Entry: ", log);
        }
        Log.d("Done", "Reading Done");

    }

    /**
     * Listener for Bottom Navigation View
     * @param menuItem
     * @return
     */

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case R.id.action_add:
                Toast.makeText(getApplicationContext(), "Clicked Add", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(MainActivity.this, AddActivity.class);
                MainActivity.this.startActivity(intent);
                break;
            case R.id.action_calendar:
                Toast.makeText(getApplicationContext(), "Clicked Calendar", Toast.LENGTH_SHORT).show();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                calendarFragment = new CalendarFragment();
                fragmentTransaction.replace(R.id.container, calendarFragment);
                fragmentTransaction.commit();
                break;
            case R.id.action_chart:
                Toast.makeText(getApplicationContext(), "Clicked Chart", Toast.LENGTH_SHORT).show();
                fragmentTransaction = fragmentManager.beginTransaction();
                reportFragment = new ReportFragment();
                fragmentTransaction.replace(R.id.container, reportFragment);
                fragmentTransaction.commit();
                break;
            case R.id.action_category:

                Toast.makeText(getApplicationContext(), "Clicked Category", Toast.LENGTH_SHORT).show();
                fragmentTransaction = fragmentManager.beginTransaction();
                categoryFragment = new CategoryFragment();
                fragmentTransaction.replace(R.id.container, categoryFragment);
                fragmentTransaction.commit();
                break;
            case R.id.action_account:
                Toast.makeText(getApplicationContext(), "Clicked Account", Toast.LENGTH_SHORT).show();
                fragmentTransaction = fragmentManager.beginTransaction();
                accountFragment = new AccountFragment();
                fragmentTransaction.replace(R.id.container, accountFragment);
                fragmentTransaction.commit();
                break;
        }
        return false;
    }
}
