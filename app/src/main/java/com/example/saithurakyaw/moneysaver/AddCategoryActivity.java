package com.example.saithurakyaw.moneysaver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class AddCategoryActivity extends AppCompatActivity {

    private EditText newCategoryName;
    private RadioGroup newCategoryType;
    private TextView backCategory, addCategory;
    private DatabaseHandler databaseHandler = new DatabaseHandler(this);
    private Category category;

    private int type = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_category);

        newCategoryName = (EditText) findViewById(R.id.newCategoryName);
        newCategoryType = (RadioGroup) findViewById(R.id.newCategoryType);
        backCategory = (TextView) findViewById(R.id.backCategory);
        addCategory = (TextView) findViewById(R.id.addCategory);

        newCategoryType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton_expense:
                        type = 1;
                        //Toast.makeText(getActivity(), "Expense", Toast.LENGTH_SHORT).show();
                        break;
                    case R.id.radioButton_income:
                        type = 0;
                        //Toast.makeText(getActivity(), "Income", Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        });

        backCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newCategoryName.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Enter Category Name", Toast.LENGTH_SHORT).show();
                }
                else{
                    Toast.makeText(getApplicationContext(), "Name :" +newCategoryName.getText().toString() +"\n Type :"+ type, Toast.LENGTH_SHORT).show();
                    databaseHandler.addCategory(new Category(newCategoryName.getText().toString(), type));
                    //databaseHandler.addCategory(new Category("Bill", 1));
                    finish();
                }
            }
        });
    }
}
