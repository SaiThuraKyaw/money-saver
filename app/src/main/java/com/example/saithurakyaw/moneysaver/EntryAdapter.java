package com.example.saithurakyaw.moneysaver;

import android.support.v7.util.DiffUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class EntryAdapter extends RecyclerView.Adapter<EntryAdapter.MyViewHolder> {

    private List<Entry> entryList;
    private List<Category> categoryList;
    private List<Account> accountList;
    private Category foundCategory;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView name, category, amount;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            category = (TextView) view.findViewById(R.id.date);
            amount = (TextView) view.findViewById(R.id.amount);
        }
    }

    public EntryAdapter(List<Entry> entryList) {
        this.entryList = entryList;
    }

    public EntryAdapter(List<Entry> entryList, List<Category> categoryList) {
        this.entryList = entryList;
        this.categoryList = categoryList;
    }

    public EntryAdapter(List<Entry> entryList, List<Category> categoryList, List<Account> accountList) {
        this.entryList = entryList;
        this.categoryList = categoryList;
        this.accountList = accountList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.entry_list_row, viewGroup, false);
        return new MyViewHolder((itemView));
    }

    @Override
    public void onBindViewHolder(MyViewHolder myViewHolder, int i) {
        Entry entry = entryList.get(i);
        myViewHolder.name.setText(entry.getEntryName());
        foundCategory = findCategory(entry.getCategoryId(),categoryList);
        myViewHolder.category.setText(foundCategory.getCategoryName());
        if(foundCategory.isExpense()==1){
            myViewHolder.amount.setTextColor(0xffff2603);
        }
        else{
            myViewHolder.amount.setTextColor(0xff00c91d);
        }
        myViewHolder.amount.setText((String.valueOf(entry.getAmount())));
    }

    @Override
    public int getItemCount() {
        return entryList.size();
    }

    public Category  findCategory(int categoryId, List<Category> categories){
        for (Category category : categories) {
            if (category.getCategoryId() == categoryId) {
                return category;
            }
        }
        return null;
    }

    public void swapItem(List<Entry> entries){
        final EntryDiffCallback entryDiffCallback = new EntryDiffCallback(this.entryList, entries);
        final DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(entryDiffCallback);

        this.entryList.clear();
        this.entryList.addAll(entries);

        diffResult.dispatchUpdatesTo(this);
    }


}
