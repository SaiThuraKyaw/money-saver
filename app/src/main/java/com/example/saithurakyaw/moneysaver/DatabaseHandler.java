package com.example.saithurakyaw.moneysaver;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class DatabaseHandler extends SQLiteOpenHelper {

    //Database Name and Version
    private static final String DATABASE_NAME = "MoneySaver";
    private static final int DATABASE_VERSION = 1;

    //Category Table
    private static final String TABLE_CATEGORY = "Category";
    private static final String CATEGORY_ID = "categoryID";
    private static final String CATEGORY_NAME = "categoryName";
    private static final String CATEGORY_TYPE = "isExpense";

    //Account Table
    private static final String TABLE_ACCOUNT = "Accoount";
    private static final String ACCOUNT_ID = "accountId";
    private static final String ACCOUNT_NAME = "accountName";
    private static final String ACCOUNT_AMOUNT = "amount";

    //Entry Table
    private static final String TABLE_ENTRY = "Entry";
    private static final String ENTRY_ID = "entryId";
    private static final String ENTRY_NAME = "entryName";
    private static final String ENTRY_DATE = "entryDate";
    private static final String ENTRY_AMOUNT = "amount";
    private static final String ENTRY_NOTE = "note";

    //Create Category Table Statement
    private static final String CREATE_CATEGORY_TABLE = "CREATE TABLE " + TABLE_CATEGORY + "(" +CATEGORY_ID + " INTEGER PRIMARY KEY, " + CATEGORY_NAME + " TEXT," + CATEGORY_TYPE + " BOOLEAN " + ")";

    //Create Account Table Statement
    private static final String CREATE_ACCOUNT_TABLE = "CREATE TABLE " + TABLE_ACCOUNT + "(" + ACCOUNT_ID + " INTEGER PRIMARY KEY, " + ACCOUNT_NAME + " TEXT," + ACCOUNT_AMOUNT + " INTEGER " + ")";

    //Create Entry Table Statement
    private static final String CREATE_ENTRY_TABLE = "CREATE TABLE " + TABLE_ENTRY + "(" + ENTRY_ID + " INTEGER PRIMARY KEY, " + ENTRY_NAME + " TEXT," + CATEGORY_ID + " INTEGER," + ACCOUNT_ID + " INTEGER," + ENTRY_DATE + " DATE," + ENTRY_AMOUNT + " INTEGER," + ENTRY_NOTE + " TEXT" + ")";

    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_ACCOUNT_TABLE);
        db.execSQL(CREATE_CATEGORY_TABLE);
        db.execSQL(CREATE_ENTRY_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ACCOUNT);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ENTRY);

        onCreate(db);
    }

    /**
     * Adding Category
     * @param category
     */
    public void addCategory(Category category){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CATEGORY_NAME, category.getCategoryName());
        values.put(CATEGORY_TYPE, category.isExpense());

        sqLiteDatabase.insert(TABLE_CATEGORY, null , values);
        sqLiteDatabase.close();
    }

    /**
     * Getting all Category
     * @return
     */
    public ArrayList<Category> getAllCategory(){
        ArrayList<Category> categoryList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY;

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Category category = new Category();
                category.setCategoryId(Integer.parseInt(cursor.getString(0)));
                category.setCategoryName(cursor.getString(1));
                category.setExpense(Integer.parseInt(cursor.getString(2)));
                categoryList.add(category);
            } while (cursor.moveToNext());
        }
        return categoryList;
    }

    public ArrayList<Category> getCategoryByType(int type){
        ArrayList<Category> categoryList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_CATEGORY + " WHERE " + CATEGORY_TYPE + " = \"" + type + "\"";

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Category category = new Category();
                category.setCategoryId(Integer.parseInt(cursor.getString(0)));
                category.setCategoryName(cursor.getString(1));
                category.setExpense(Integer.parseInt(cursor.getString(2)));
                categoryList.add(category);
            } while (cursor.moveToNext());
        }
        return categoryList;
    }

    /**
     * Updating Category
     * @param category
     * @return
     */
    public int updateCategory(Category category){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CATEGORY_NAME, category.getCategoryName());
        values.put(CATEGORY_TYPE, category.isExpense());

        return sqLiteDatabase.update(TABLE_CATEGORY, values, CATEGORY_ID + " =?", new String[] {String.valueOf(category.getCategoryId())});
    }

    /**
     * Deleting Category
     * @param categoryId
     */
    public void deleteCategory(int categoryId){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_CATEGORY, CATEGORY_ID + " = ? ", new String[]{String.valueOf(categoryId)});
        sqLiteDatabase.close();
    }

    /**
     * Adding Account
     * @param account
     */
    public void addAccount(Account account){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ACCOUNT_NAME, account.getAccountName());
        values.put(ACCOUNT_AMOUNT, account.getAmount());

        sqLiteDatabase.insert(TABLE_ACCOUNT, null , values);
        sqLiteDatabase.close();
    }



    /**
     * Getting all Account
     * @return
     */
    public ArrayList<Account> getAllAccount(){
        ArrayList<Account> accountList= new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ACCOUNT;

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Account account = new Account();
                account.setAccountId(Integer.parseInt(cursor.getString(0)));
                account.setAccountName(cursor.getString(1));
                account.setAmount(Double.parseDouble(cursor.getString(2)));
                accountList.add(account);
            } while (cursor.moveToNext());
        }
        return accountList;
    }

    /**
     * Updating Account
     * @param account
     * @return
     */
    public int updateAccount(Account account){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ACCOUNT_NAME, account.getAccountName());
        values.put(ACCOUNT_AMOUNT, account.getAmount());

        return sqLiteDatabase.update(TABLE_ACCOUNT, values, ACCOUNT_ID + " = ? ", new String[] {String.valueOf(account.getAccountId())});
    }

    /**
     * Deleting Account
     * @param account
     */
    public void deleteAccount(Account account){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_ACCOUNT, ACCOUNT_ID + " = ? ", new String[]{String.valueOf(account.getAccountId())});
        sqLiteDatabase.close();
    }

    /**
     * Add Entry List
     * @param entry
     */
    public void addEntryList(Entry entry){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ENTRY_NAME, entry.getEntryName());
        values.put(CATEGORY_ID, entry.getCategoryId());
        values.put(ACCOUNT_ID, entry.getAccountId());
        values.put(ENTRY_DATE, entry.getEntryDate());
        values.put(ENTRY_AMOUNT, entry.getAmount());
        values.put(ENTRY_NOTE, entry.getNote());

        sqLiteDatabase.insert(TABLE_ENTRY, null , values);
        sqLiteDatabase.close();
    }

    /**
     * Get All Entry List
     * @return
     */
    public ArrayList<Entry> getAllEntryList(){
        ArrayList<Entry> entryList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY;

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Entry entry = new Entry();
                entry.setEntryId(Integer.parseInt(cursor.getString(0)));
                entry.setEntryName(cursor.getString(1));
                entry.setCategoryId(Integer.parseInt(cursor.getString(2)));
                entry.setAccountId((Integer.parseInt(cursor.getString(3))));
                entry.setEntryDate(cursor.getString(4));
                entry.setAmount(Double.parseDouble(cursor.getString(5)));
                entry.setNote(cursor.getString(6));
                entryList.add(entry);
            }while (cursor.moveToNext());
        }
        return entryList;
    }

    public ArrayList<Entry> getEntryListByDate(String date){
        ArrayList<Entry> entryList = new ArrayList<>();
        //String selectQuery = "SELECT  * FROM " + TABLE_TODO + " WHERE " + KEY_ID + " = " + todo_id;
        String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE " + ENTRY_DATE + " = \"" + date + "\"";

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Entry entry = new Entry();
                entry.setEntryId(Integer.parseInt(cursor.getString(0)));
                entry.setEntryName(cursor.getString(1));
                entry.setCategoryId(Integer.parseInt(cursor.getString(2)));
                entry.setAccountId((Integer.parseInt(cursor.getString(3))));
                entry.setEntryDate(cursor.getString(4));
                entry.setAmount(Double.parseDouble(cursor.getString(5)));
                entry.setNote(cursor.getString(6));
                entryList.add(entry);
            }while (cursor.moveToNext());
        }
        return entryList;
    }

    public ArrayList<Entry> getEntryByMonth(String sDate, String eDate){
        ArrayList<Entry> entryList = new ArrayList<>();
        //String selectQuery = "SELECT  * FROM " + TABLE_TODO + " WHERE " + KEY_ID + " = " + todo_id;
        //String selectQuery = "SELECT * FROM " + TABLE_ENTRY + " WHERE " + ENTRY_DATE + " >= \"" + sDate +  "\"" + " AND "+ ENTRY_DATE + " <= \"" + eDate + "\"";
        String selectQuery = "SELECT " + CATEGORY_ID +", SUM(" + ENTRY_AMOUNT + ")" + " FROM " + TABLE_ENTRY + " WHERE " + ENTRY_DATE + " >= \"" + sDate +  "\"" + " AND "+ ENTRY_DATE + " <= \"" + eDate + "\"" + " GROUP BY " + CATEGORY_ID;

        Log.d("Entry By Month", selectQuery);
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()){
            do{
                Entry entry = new Entry();
                //entry.setEntryId(Integer.parseInt(cursor.getString(0)));
                //entry.setEntryName(cursor.getString(1));
                entry.setCategoryId(Integer.parseInt(cursor.getString(0)));
                //entry.setAccountId((Integer.parseInt(cursor.getString(3))));
                //entry.setEntryDate(cursor.getString(4));
                entry.setAmount(Double.parseDouble(cursor.getString(1)));
                //entry.setNote(cursor.getString(6));
                entryList.add(entry);
            }while (cursor.moveToNext());
        }
        return entryList;
    }



    /**
     * Update Entry List
     * @param entry
     * @return
     */
    public int updateEntryList(Entry entry){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ENTRY_NAME, entry.getEntryName());
        values.put(CATEGORY_ID, entry.getCategoryId());
        values.put(ACCOUNT_ID, entry.getAccountId());
        values.put(ENTRY_DATE, entry.getEntryDate());
        values.put(ENTRY_AMOUNT, entry.getAmount());
        values.put(ENTRY_NOTE, entry.getNote());

        return sqLiteDatabase.update(TABLE_ENTRY, values, ENTRY_ID + " = ? ", new String[]{String.valueOf((entry.getEntryId()))});
    }

    /**
     * Delete Entry List
     * @param entry
     */
    public void deleteEntryList(Entry entry){
        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        sqLiteDatabase.delete(TABLE_ENTRY, ENTRY_ID + " = ? ", new String[]{String.valueOf(entry.getEntryId())});
        sqLiteDatabase.close();
    }
}
