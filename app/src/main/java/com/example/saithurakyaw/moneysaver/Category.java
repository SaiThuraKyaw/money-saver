package com.example.saithurakyaw.moneysaver;

public class Category {
    private int categoryId;
    private String categoryName;
    private int isExpense;

    public Category() {
    }

    public Category(int categoryId, String categoryName, int isExpense) {
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.isExpense = isExpense;
    }

    public Category(String categoryName, int isExpense) {

        this.categoryName = categoryName;
        this.isExpense = isExpense;
    }

    public Category(int categoryId) {
        this.categoryId = categoryId;
    }

    public int isExpense() {
        return isExpense;
    }

    public void setExpense(int expense) {
        isExpense = expense;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
