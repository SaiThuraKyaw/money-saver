package com.example.saithurakyaw.moneysaver;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.CalendarMode;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CalendarFragment extends Fragment{


    public MaterialCalendarView calendarView;
    private static final int SWIPE_THRESHOLD = 100;
    private static final int SWIPE_VELOCITY_THRESHOLD = 100;

    private TextView dateView;
    private TextView totalAmountView;

    private List<Entry> entryList = new ArrayList<>();
    private List<Category> categoryList = new ArrayList<>();
    private List<Account> accountList = new ArrayList<>();
    private RecyclerView recyclerView;
    private EntryAdapter entryAdapter;
    private DatabaseHandler databaseHandler;
    private SimpleDateFormat simpleDateFormat = new java.text.SimpleDateFormat("yyyy-M-d");
    private String currentDate;
    private double totalAmount = 0.0;

    public CalendarFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_calendar, container, false);

        calendarView = (MaterialCalendarView) v.findViewById(R.id.calendarView);
        dateView = (TextView) v.findViewById(R.id.dateView);
        totalAmountView = (TextView) v.findViewById(R.id.totalAmountView);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);

        Date dt = new java.util.Date();
        currentDate = simpleDateFormat.format(dt);
        databaseHandler = new DatabaseHandler(v.getContext());
        entryList = databaseHandler.getEntryListByDate(currentDate);
        categoryList = databaseHandler.getAllCategory();
        accountList = databaseHandler.getAllAccount();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        entryAdapter = new EntryAdapter(entryList, categoryList, accountList);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(v.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(entryAdapter);

        calendarView.setSelectedDate(CalendarDay.today());
        getTotalAmount(currentDate);

        calendarView.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(@NonNull MaterialCalendarView widget, @NonNull CalendarDay date, boolean selected) {
                totalAmount = 0.0;
                String dateString = date.getYear()+"-"+date.getMonth()+"-"+date.getDay();
                Toast.makeText(getActivity(), dateString, Toast.LENGTH_SHORT).show();
                getTotalAmount(dateString);
                entryAdapter.swapItem(entryList);
            }
        });


        v.setOnTouchListener(new OnSwipeTouchListener(v.getContext()) {
            @Override
            public void onSwipeDown() {
                calendarMoodMonth();
                Toast.makeText(getActivity(), "Down", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onSwipeUp() {
                calendarMoodWeek();
                Toast.makeText(getActivity(), "Up", Toast.LENGTH_SHORT).show();
            }
        });


        return v;
    }


    public void calendarMoodMonth(){
        calendarView.state().edit().setCalendarDisplayMode(CalendarMode.MONTHS).commit();
        Toast.makeText(getActivity(), calendarView.getCalendarMode().toString() , Toast.LENGTH_SHORT).show();
    }

    public void calendarMoodWeek(){
        calendarView.state().edit().setCalendarDisplayMode(CalendarMode.WEEKS).commit();
        Toast.makeText(getActivity(), calendarView.getCalendarMode().toString() , Toast.LENGTH_SHORT).show();
    }


    public void getTotalAmount(String date){
        entryList = databaseHandler.getEntryListByDate(date);
        dateView.setText(String.valueOf(date));
        for(Entry entry: entryList)
        {
            totalAmount += entry.getAmount();
            if(totalAmount < 0){
                totalAmountView.setTextColor(0xffff2603);
            }
            else{
                totalAmountView.setTextColor(0xff00c91d);
            }
        }
        totalAmountView.setText(String.valueOf(String.valueOf(totalAmount)));
    }
}
