package com.example.saithurakyaw.moneysaver;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {


    private AccountAdapter accountAdapter;
    private DatabaseHandler databaseHandler;
    private TextView addAccount;

    private RecyclerView recyclerView;

    private List<Account> accountList = new ArrayList<>();

    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_account, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        addAccount = (TextView) v.findViewById(R.id.addAccount);

        databaseHandler = new DatabaseHandler(v.getContext());
        accountList = databaseHandler.getAllAccount();

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        accountAdapter = new AccountAdapter(accountList);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(v.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(accountAdapter);

        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddAccountActivity.class);
                getActivity().startActivity(intent);
            }
        });

        return v;
    }

}
