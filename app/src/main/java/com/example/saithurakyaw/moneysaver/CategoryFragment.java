package com.example.saithurakyaw.moneysaver;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class CategoryFragment extends Fragment {


    private CategoryAdapter categoryAdapter;
    private DatabaseHandler databaseHandler;

    private RecyclerView recyclerView;
    private RadioGroup radioGroup;
    private TextView addCategory;


    private List<Category> categoryList = new ArrayList<>();
    private int type = 1;

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_category, container, false);

        recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
        addCategory = (TextView) v.findViewById(R.id.addCategory);

        databaseHandler = new DatabaseHandler(v.getContext());
        categoryList = databaseHandler.getCategoryByType(type);

        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        categoryAdapter = new CategoryAdapter(categoryList);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new DividerItemDecoration(v.getContext(), LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(categoryAdapter);

        radioGroup = (RadioGroup) v.findViewById(R.id.category_radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radioButton_expense:
                        type = 1;
                        //Toast.makeText(getActivity(), "Expense", Toast.LENGTH_SHORT).show();

                        break;
                    case R.id.radioButton_income:
                        type = 0;
                        //Toast.makeText(getActivity(), "Income", Toast.LENGTH_SHORT).show();
                        break;
                }
                categoryList = databaseHandler.getCategoryByType(type);
                categoryAdapter.swapItem(categoryList);

            }
        });
        addCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), AddCategoryActivity.class);
                getActivity().startActivity(intent);
            }
        });
        return v;
    }

}
