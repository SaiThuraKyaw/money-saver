package com.example.saithurakyaw.moneysaver;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AccountAdapter extends RecyclerView.Adapter<AccountAdapter.MyViewHolder>{

    private List<Account> accountList;

    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView text_accountName, text_amount;


        public MyViewHolder(View view) {
            super(view);
            text_accountName = (TextView) view.findViewById(R.id.text_accountName);
            text_amount = (TextView) view.findViewById(R.id.text_amount);
        }
    }

    public AccountAdapter(List<Account> accountList) {
        this.accountList = accountList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.account_list_row, viewGroup, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, int i) {
        Account account = accountList.get(i);
        myViewHolder.text_accountName.setText(account.getAccountName());
        myViewHolder.text_amount.setText(String.valueOf(account.getAmount()));

    }

    @Override
    public int getItemCount() {
        return accountList.size();
    }
}
