package com.example.saithurakyaw.moneysaver;

import android.app.DatePickerDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddActivity extends AppCompatActivity {

    private List<Category> categoryList = new ArrayList<>();
    private List<Account> accountList = new ArrayList<>();
    private  Account foundAccount;
    private DatabaseHandler databaseHandler;

    private Spinner categorySpinner;
    private Spinner accountSpinner;
    private ImageButton addButton;
    private ImageButton cancelButton;
    private RadioGroup radioGroup;
    private TextView datePickerDialogButton;
    private EditText textName;
    private EditText textAmount;

    private int year, month, day;
    private int expenseType = 1;

    private String name, date;
    private int categoryId,plusMonth;
    private double price;
    private static DecimalFormat df2 = new DecimalFormat(".##");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        this.showDatePickerDialog();

        addButton = (ImageButton) findViewById(R.id.addButton);
        cancelButton = (ImageButton) findViewById(R.id.cancelButton);
        textName = (EditText) findViewById(R.id.textName);
        textAmount = (EditText) findViewById(R.id.textAmount);
        //datePickerDialogButton = (TextView)findViewById(R.id.datePickerDialogButton);

        // Get current year, month and day.
        Calendar now = Calendar.getInstance();
        year = now.get(java.util.Calendar.YEAR);
        month = (now.get(java.util.Calendar.MONTH));
        day = now.get(java.util.Calendar.DAY_OF_MONTH);

        plusMonth = month +1;
        datePickerDialogButton.setText(day+ "/" + plusMonth +"/"+year);
        date = year+"-"+ plusMonth +"-"+day;


        databaseHandler = new DatabaseHandler(this);
        accountList = databaseHandler.getAllAccount();
        addCategoryToSpinner(expenseType);
        addAccountToSpinner();

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.expense:
                        Toast.makeText(getApplicationContext(), "Expense", Toast.LENGTH_SHORT).show();
                        expenseType = 1;
                        break;
                    case R.id.income:
                        Toast.makeText(getApplicationContext(), "Income", Toast.LENGTH_SHORT).show();
                        expenseType = 0;
                        break;
                }
                addCategoryToSpinner(expenseType);
            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), String.valueOf(accountSpinner.getSelectedItem())+ " / " +
                        String.valueOf(categorySpinner.getSelectedItem()), Toast.LENGTH_SHORT).show();
                name = String.valueOf(textName.getText());
                categoryId = findCategoryId(String.valueOf(categorySpinner.getSelectedItem()), categoryList);

                if(textName.getText().toString().equals("")){
                    name = "-";
                }
                if(textAmount.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Add Amount Please", Toast.LENGTH_SHORT).show();
                }
                else {
                    price = Double.parseDouble(textAmount.getText().toString());
                    foundAccount = findAccount(String.valueOf(accountSpinner.getSelectedItem()), accountList);
                    //Expense
                    if (expenseType == 1) {
                        price *= -1;
                        Toast.makeText(getApplicationContext(), "Name : " + name + "\nCategory :" + categoryId + "\nAccount :" + foundAccount.getAccountId() + "\nAccount Name :" + foundAccount.getAccountName() + "\nAmount :" + foundAccount.getAmount()
                                + "\nDate :" + date + "\nAmount :" + df2.format(price), Toast.LENGTH_SHORT).show();
                        // Entry entry = new ()
                        databaseHandler.addEntryList(new Entry(name,categoryId,foundAccount.getAccountId(),date, price, ""));
                        databaseHandler.updateAccount(new Account(foundAccount.getAccountId(), foundAccount.getAccountName(), foundAccount.getAmount()+price));

                    }
                    //Income
                    else if (expenseType == 0) {
                        Toast.makeText(getApplicationContext(), "Name : " + name + "\nCategory :" + categoryId + "\nAccount :" + foundAccount.getAccountId() + "\nAccount Name :" + foundAccount.getAccountName() + "\nAmount :" + foundAccount.getAmount()
                                + "\nDate :" + date + "\nAmount :" + df2.format(price), Toast.LENGTH_SHORT).show();
                        databaseHandler.addEntryList(new Entry(name,categoryId,foundAccount.getAccountId(),date, price, ""));
                        databaseHandler.updateAccount(new Account(foundAccount.getAccountId(), foundAccount.getAccountName(), foundAccount.getAmount()+price));
                    }
                    finish();
                }
            }
        });

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
    private void showDatePickerDialog()
    {
        datePickerDialogButton = (TextView)findViewById(R.id.datePickerDialogButton);
        // Get open DatePickerDialog button.
        datePickerDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Create a new OnDateSetListener instance. This listener will be invoked when user click ok button in DatePickerDialog.
                DatePickerDialog.OnDateSetListener onDateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        month += 1;
                        String selectedDate = dayOfMonth + "/" + month + "/" +year;
                        date = year + "-" + month + "-" +dayOfMonth;
                        Toast.makeText(getApplicationContext(), selectedDate , Toast.LENGTH_SHORT).show();
                        datePickerDialogButton.setText(selectedDate);
                    }
                };
                // Create the new DatePickerDialog instance.
                DatePickerDialog datePickerDialog = new DatePickerDialog(AddActivity.this, onDateSetListener, year, month, day);

                // Popup the dialog.
                datePickerDialog.show();
            }
        });
    }

    private void addCategoryToSpinner(int isExpense){
        categoryList = databaseHandler.getCategoryByType(isExpense);
        categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        List<String> list = new ArrayList<String>();
        for(Category category: categoryList){
            list.add(category.getCategoryName());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        categorySpinner.setAdapter(dataAdapter);
    }

    private void addAccountToSpinner(){
        accountSpinner = (Spinner) findViewById(R.id.accountSpinner);
        List<String> list = new ArrayList<>();
        for(Account account: accountList){
            list.add(account.getAccountName());
        }
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        accountSpinner.setAdapter(dataAdapter);
    }

    public int  findCategoryId(String  categoryName, List<Category> categories){
        for (Category category : categories) {
            if (category.getCategoryName().equals(categoryName)) {
                return category.getCategoryId();
            }
        }
        return 0;
    }

    public Account findAccount(String  accountName, List<Account> accounts){
        for (Account account : accounts) {
            if (account.getAccountName().equals(accountName)) {
                return account;
            }
        }
        return null;
    }

}
