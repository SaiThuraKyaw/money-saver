package com.example.saithurakyaw.moneysaver;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class AddAccountActivity extends AppCompatActivity {

    private TextView backAccount, addAccount;
    private EditText newAccountName, newAmount;
    private DatabaseHandler databaseHandler = new DatabaseHandler(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_account);

        backAccount = (TextView) findViewById(R.id.backAccount);
        addAccount = (TextView) findViewById(R.id.addAccount);
        newAccountName = (EditText) findViewById(R.id.newAccountName);
        newAmount = (EditText) findViewById(R.id.newAmount);

        backAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        addAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(newAccountName.getText().toString().equals("") || newAmount.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(), "Please Enter all the Fields", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    databaseHandler.addAccount(new Account(newAccountName.getText().toString(), Double.parseDouble(newAmount.getText().toString())));
                    Toast.makeText(getApplicationContext(), "Name :" + newAccountName.getText() + "\nAmount :" +newAmount.getText(), Toast.LENGTH_SHORT).show();
                    finish();

                }
            }
        });
    }
}
