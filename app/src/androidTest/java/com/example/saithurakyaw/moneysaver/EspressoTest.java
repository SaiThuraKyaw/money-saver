package com.example.saithurakyaw.moneysaver;

import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;


@SmallTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EspressoTest {

    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void test01NewExpenseCategory() {
        onView(withId(R.id.action_category)).perform(click());
        onView(withId(R.id.addCategory)).perform(click());
        onView(withId(R.id.newCategoryName)).perform(typeText("Eating"), closeSoftKeyboard());
        onView(withId(R.id.addCategory)).perform(click());
        onView(withId(R.id.action_category)).perform(click());
        onView(withId(R.id.radioButton_income)).perform(click());
        onView(withId(R.id.radioButton_expense)).perform(click());
        onView(withId(R.id.categoryName)).check(matches(withText("Eating")));
    }

    @Test
    public void test02NewIncomeCategory() {
        onView(withId(R.id.action_category)).perform(click());
        onView(withId(R.id.addCategory)).perform(click());
        onView(withId(R.id.newCategoryName)).perform(typeText("Salary"), closeSoftKeyboard());
        onView(withId(R.id.radioButton_income)).perform(click());
        onView(withId(R.id.addCategory)).perform(click());
        onView(withId(R.id.action_category)).perform(click());
        onView(withId(R.id.radioButton_income)).perform(click());
        onView(withId(R.id.categoryName)).check(matches(withText("Salary")));
    }

    @Test
    public void test03NewAccount() {
        onView(withId(R.id.action_account)).perform(click());
        onView(withId(R.id.addAccount)).perform(click());
        onView(withId(R.id.newAccountName)).perform(typeText("Default"), closeSoftKeyboard());
        onView(withId(R.id.newAmount)).perform(typeText("1000"), closeSoftKeyboard());
        onView(withId(R.id.addAccount)).perform(click());
    }

    @Test
    public void test04SecondNewAccount() {
        onView(withId(R.id.action_account)).perform(click());
        onView(withId(R.id.addAccount)).perform(click());
        onView(withId(R.id.newAccountName)).perform(typeText("Bank"), closeSoftKeyboard());
        onView(withId(R.id.newAmount)).perform(typeText("1000"), closeSoftKeyboard());
        onView(withId(R.id.addAccount)).perform(click());
    }

    @Test
    public void test05AddExpense() {
        onView(withId(R.id.action_add)).perform(click());
        onView(withId(R.id.textName)).perform(typeText("Breakfast"), closeSoftKeyboard());
        onView(withId(R.id.textAmount)).perform(typeText("3"), closeSoftKeyboard());
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.action_add)).perform(click());
    }

    @Test
    public void test06CheckAccountAfterAddingExpense() {
        onView(withId(R.id.action_account)).perform(click());
        onView(withId(R.id.recycler_view)).check(matches(hasDescendant(withText("997.0"))));
    }

    @Test
    public void test07AddAnotherExpense() {
        onView(withId(R.id.action_add)).perform(click());
        onView(withId(R.id.textName)).perform(typeText("Lunch"), closeSoftKeyboard());
        onView(withId(R.id.textAmount)).perform(typeText("15"), closeSoftKeyboard());
        onView(withId(R.id.addButton)).perform(click());
        onView(withId(R.id.action_add)).perform(click());
    }

    @Test
    public void test08CheckAccountAfterAddingExpense() {
        onView(withId(R.id.action_account)).perform(click());
        onView(withId(R.id.recycler_view)).check(matches(hasDescendant(withText("982.0"))));
    }

    @Test
    public void test09AddIncome(){
        onView(withId(R.id.action_add)).perform(click());
        onView(withId(R.id.textName)).perform(typeText("Salary"), closeSoftKeyboard());
        onView(withId(R.id.textAmount)).perform(typeText("3000"), closeSoftKeyboard());
        onView(withId(R.id.income)).perform(click());
        onView(withId(R.id.addButton)).perform(click());
    }

    @Test
    public void test10CheckAccountAfterAddingIncome(){
        onView(withId(R.id.action_account)).perform(click());
        onView(withId(R.id.recycler_view)).check(matches(hasDescendant(withText("3982.0"))));
    }
}
